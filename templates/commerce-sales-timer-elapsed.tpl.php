<?php

/**
 * @file
 * Theme implementation to display an elapsed timer.
 *
 * Variables needed to make countdown work:
 * %D - days
 * %H - hours
 * %M - minutes
 * %S - seconds
 *
 * For more variables and customizations refer:
 * http://hilios.github.io/jQuery.countdown/documentation.html
 */
?>

<div class="timer timer--elapsed">
  <!-- Timer elapsed message. -->
  <div class="timer__message">
    <?php print t('Sale over') ?>
  </div>
</div>
