<?php

/**
 * @file
 * Theme implementation to display a ticking timer.
 *
 * Variables needed to make countdown work:
 * %D - days
 * %H - hours
 * %M - minutes
 * %S - seconds
 *
 * For more variables and customizations refer:
 * http://hilios.github.io/jQuery.countdown/documentation.html
 */
?>
<!-- Text before timer. -->
<div class="timer-before">
  <?php print t('Only') ?>
</div>

<div class="timer timer--ticking">
  <!-- Day. -->
  <div class="timer__time timer__time--day">
    <div class="timer__time__top">
      %D
    </div>
    <div class="timer__time__bottom">
      <?php print t('Days') ?>
    </div>
  </div>

  <!-- Hour. -->
  <div class="timer__time timer__time--hour">
    <div class="timer__time__top">
      %H
    </div>
    <div class="timer__time__bottom">
      <?php print t('Hrs') ?>
    </div>
  </div>

  <!-- Minute. -->
  <div class="timer__time timer__time--min">
    <div class="timer__time__top">
      %M
    </div>
    <div class="timer__time__bottom">
      <?php print t('Min') ?>
    </div>
  </div>

  <!-- Second. -->
  <div class="timer__time timer__time--sec">
    <div class="timer__time__top">
      %S
    </div>
    <div class="timer__time__bottom">
      <?php print t('Sec') ?>
    </div>
  </div>
</div>

<!-- Text after timer. -->
<div class="timer-after">
  <?php print t('Left') ?>
</div>
