<?php

/**
 * @file
 * Theme implementation to display extra links on node view.
 *
 * Available variables:
 * - $node: Sales popup node.
 */
?>
<div class="commerce-sales-popup__extra-links">
  <ul>
    <li><a href="<?php print $preview; ?>">Preview</a></li>
    <li><a href="<?php print $publish; ?>">Publish</a></li>
  </ul>

  <div class="commerce-sales-popup__not-published">New changes made to sales popup information are not published yet. Click Publish to make them available.</div>
</div>
