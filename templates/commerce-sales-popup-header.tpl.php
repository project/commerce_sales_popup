<?php

/**
 * @file
 * Theme implementation to display sales popup header.
 *
 * Available variables:
 * - $node: Sales popup node.
 */

if (!empty($node->sales_popup_header_image[LANGUAGE_NONE][0]['uri'])) {
  $image = image_style_url('commerce_sales_popup_header', $node->sales_popup_header_image[LANGUAGE_NONE][0]['uri']);
}
$cta = $node->sales_popup_cta[LANGUAGE_NONE][0]['value'];
$cta_label = $node->body[LANGUAGE_NONE][0]['value'];
$classes = "commerce-sales-popup__header commerce-sales-popup__header--" . $node->nid;
?>

<div class="<?php print $classes; ?>">
  <div class="commerce-sales-popup__header__container">
    <?php if(!empty($cta)): ?>
      <a href="<?php print $cta ?>">
    <?php endif; ?>

    <div class="commerce-sales-popup__header__banner">
      <?php if (isset($image)): ?>
        <img src="<?php print $image ?>">
      <?php endif; ?>
    </div>

    <div class="commerce-sales-popup__header__text">
      <?php print $cta_label; ?>
    </div>

    <div class="commerce-sales-popup__countdown"></div>

    <?php if(!empty($cta)): ?>
      </a>
    <?php endif; ?>
  </div>
</div>
