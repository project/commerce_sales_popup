<?php

/**
 * @file
 * Theme implementation to display sales popup modal.
 *
 * Available variables:
 * - $node: Sales popup node.
 */

$title = $node->title;
if (!empty($node->sales_popup_modal_image[LANGUAGE_NONE][0]['uri'])) {
  $image = image_style_url('commerce_sales_popup_modal', $node->sales_popup_modal_image[LANGUAGE_NONE][0]['uri']);
}
$text = $node->body[LANGUAGE_NONE][0]['value'];
$cta = $node->sales_popup_cta[LANGUAGE_NONE][0]['value'];
$cta_label = $node->sales_popup_cta_label[LANGUAGE_NONE][0]['value'];
?>

<div class="commerce-sales-popup__modal__container">
  <?php if(!empty($cta)): ?>
    <a href="<?php print $cta ?>">
  <?php endif; ?>

  <div class="commerce-sales-popup__modal__banner">
    <?php if(isset($image)): ?>
      <img src="<?php print $image ?>">
    <?php endif; ?>
  </div>

  <div class="commerce-sales-popup__modal__text">
    <?php print $text; ?>
  </div>

  <div class="commerce-sales-popup__modal__countdown">
    <div class="commerce-sales-popup__countdown"></div>
  </div>


  <div class="commerce-sales-popup__modal__cta">
    <div class="commerce-sales-popup__modal__cta-button">
      <?php print $cta_label; ?>
    </div>
  </div>

  <?php if(!empty($cta)): ?>
    </a>
  <?php endif; ?>
</div>
