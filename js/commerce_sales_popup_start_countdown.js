(function ($, Drupal) {
  Drupal.behaviors.commerce_sales_popup_countdown = {
    attach: function (context, settings) {
      var final_date = Drupal.settings.commerce_sales_popup.final_date;
      var ticking = Drupal.settings.commerce_sales_popup.timer_ticking;
      var elapsed = Drupal.settings.commerce_sales_popup.timer_elapsed;

      $('div[class^="commerce-sales-popup__countdown"]').countdown(final_date, function(event) {
        if (event.elapsed) {
          $(this).html(event.strftime(elapsed));
        }
        else {
          $(this).html(event.strftime(ticking));
        }
      });
    }
  };
})(jQuery, Drupal);
