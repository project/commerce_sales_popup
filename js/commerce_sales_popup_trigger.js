/**
 * @file
 * Trigger sales popup modal.
 */

/**
 * Drupal behavior will lead to an infinite loop of popup displays
 * caused by click(), so cannot be used to trigger ctools modal here.
 * Going the old fashioned way with document ready() event.
 */
(function ($) {
  $(document).ready(function() {
    var pathname = window.location.pathname;
    var cookie_name = 'sales_modal_processed_' + pathname;
    var display_processed = $.cookie(cookie_name);

    // Display modal only once per session.
    if (display_processed != 1) {
      var settings = $(Drupal.settings.commerce_sales_popup_modal);
      var date = new Date();

      $.each(settings, function(key, value) {
        var $link = $(value.popup_link);
        var selector = '.commerce-sales-popup--' + value.node_id + ' a.ctools-modal-sales-popup-style';
        var $modal_link = $(selector);
        var url = Drupal.settings.basePath + 'commerce_sales_popup/nojs/' + value.node_id;

        if (value.node_id != 'undefined') {
          $link.appendTo('body');
          $modal_link.click(Drupal.CTools.Modal.clickAjaxLink);
          Drupal.ajax[$modal_link.attr('href')] = new Drupal.ajax(url, $(selector), {
            url: url,
            event: 'click'
          });
          $(selector).click();
        }
      });

      // Set cookie for 5 minutes.
      date.setTime(date.getTime() + (5 * 60 * 1000));
      $.cookie(cookie_name, 1, {expires: date});
    }
  });
}(jQuery));
