(function ($, Drupal) {
  Drupal.behaviors.commerce_sales_popup_top_bar = {
    attach: function (context, settings) {

      var settings = Drupal.settings.commerce_sales_popup_header;
      $.each(settings, function(key, value) {
        var $topBar = $(value.top_bar);
        var nid = value.nid;

        if (nid != 'undefined') {
          if ($('.commerce-sales-popup__header--' + nid).length == 0) {
            $topBar.prependTo('body');
          }
        }
      });
    }
  };
})(jQuery, Drupal);
