Commerce Sales popup
--------------------
This module is not at all specific to Drupal commerce. It can be used for many use cases
where you want to display a modal with the countdown timer.


Installation
------------
- Download **jQuery.countdown** js library
- Download the latest version of this library from : http://hilios.github.io/jQuery.countdown/
- After extracting, place the library in the /libraries folder so the path looks like:
*/sites/all/libraries/jquery-countdown*
- Finally, enable the module.


Requiremnents
-------------
- Date module
- cTools module
- Libraries module
- jQuery (>=1.7)


Usage
-----
- Use *commerce_sales_popup* content type to create a new sale popup.
- After creating this node, you will get two options - Preview, and Publish.
- Use *Preview* to preview your saved changes irrespective of paths.
- Use *Publish* to immediately publish your changes to the site.
- For now, ***Publish*** is global, and will publish any other sale right away.


Troubleshooting tips
--------------------
- If you do not see modal popup after repeated attempts, its a fair possibility it will not show up after first page
  load. By default, modal popup is set to hide for 5 minutes so that it does not pop up repeatedly
  on same page on reload, and disturb site UX.
- If still no modal popup, or top bar, check dates - Sale date should be greater than now less than some future date.
- If nothing really happens - no modal, no top bar - check jQuery.countdown library is installed properly or not.


Maintainer
----------
gauravjeet (https://www.drupal.org/u/gauravjeet)


Theming and styles
------------------
Hubbs (https://www.drupal.org/u/hubbs)


Supporting organization
-----------------------
Acro Media Inc.
