<?php
/**
 * @file
 * Display sales popup as modal, or/and as header on desired pages.
 */

define('SALES_ENTITY_TYPE', 'commerce_sales_popup');
define('COUNTDOWN_LIBRARY', 'jquery-countdown');

/**
 *  Implements of hook_menu()
 */
function commerce_sales_popup_menu() {
  $items['commerce_sales_popup/%ctools_js/%'] = [
    'title' => 'Sales popup Modal',
    'page arguments' => [1, 2],
    'page callback' => 'commerce_sales_popup_modal',
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
  ];
  $items['commerce-sales-popup-extra/preview/%node/%'] = [
    'title' => 'Commerce sales preview',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['commerce_sales_popup_preview', 2, 3],
    'access arguments' => ['administer content'],
    'type' => MENU_NORMAL_ITEM,
  ];
  $items['commerce-sales-popup-extra/publish/%node'] = [
    'page callback' => 'drupal_get_form',
    'page arguments' => ['commerce_sales_popup_publish', 2],
    'access arguments' => ['administer content'],
    'type' => MENU_LOCAL_TASK,
  ];

  return $items;
}

/**
 * Implements hook_init().
 */
function commerce_sales_popup_init() {
  $mappings = get_display_pages();
  if (!empty($mappings)) {
    $wrappers = is_desired_path($mappings);

    if (!empty($wrappers)) {
      if (($library = libraries_detect(COUNTDOWN_LIBRARY)) && !empty($library['installed'])) {

        // If paths set, display multiple sales on same page.
        foreach ($wrappers as $key => $wrapper) {
          if (commerce_sale_validate_period($wrapper)) {
            if ($wrapper->__isset('sales_display_type')) {
              switch ($wrapper->sales_display_type->value()) {
                case 'popup':
                  commerce_sales_display_popup_modal($wrapper, $key);
                  break;
                case 'header':
                  commerce_sales_display_popup_header($wrapper, $key);
                  break;
                case 'both':
                  commerce_sales_display_popup_both($wrapper);
                  break;
              }
            }
          }
        }
      }
      else {
        watchdog('commerce_sales_popup', $library['error message'], [], WATCHDOG_ERROR);
      }
    }
  }
}

/**
 * Implements hook_libraries_info().
 */
function commerce_sales_popup_libraries_info() {
  $libraries[COUNTDOWN_LIBRARY] = [
    'name' => 'jQuery.countdown',
    'vendor url' => 'http://hilios.github.io/jQuery.countdown/',
    'download url' => 'http://hilios.github.io/jQuery.countdown/',
    'files' => [
      'js' => ['jquery.countdown.min.js'],
    ],
    'version arguments' => [
      'file' => 'jquery.countdown.min.js',
      'pattern' => '/v(\d+)\.(\d+)\.(\d+)/',
      'lines' => 5,
    ],
  ];
  return $libraries;
}

/**
 * Implements hook_theme().
 */
function commerce_sales_popup_theme() {
  $items['commerce_sales_popup_modal'] = [
    'template' => 'commerce-sales-popup-modal',
    'path' => drupal_get_path('module', 'commerce_sales_popup') . '/templates',
    'variables' => ['node' => NULL],
  ];
  $items['commerce_sales_popup_header'] = [
    'template' => 'commerce-sales-popup-header',
    'path' => drupal_get_path('module', 'commerce_sales_popup') . '/templates',
    'variables' => ['node' => NULL],
  ];
  $items['commerce_sales_extra_links'] = [
    'template' => 'commerce-sales-extra-links',
    'path' => drupal_get_path('module', 'commerce_sales_popup') . '/templates',
    'variables' => [
      'preview' => NULL,
      'publish' => NULL,
    ],
  ];
  $items['commerce_sales_timer_ticking'] = [
    'template' => 'commerce-sales-timer-ticking',
    'path' => drupal_get_path('module', 'commerce_sales_popup') . '/templates',
    'variables' => [],
  ];
  $items['commerce_sales_timer_elapsed'] = [
    'template' => 'commerce-sales-timer-elapsed',
    'path' => drupal_get_path('module', 'commerce_sales_popup') . '/templates',
    'variables' => [],
  ];

  return $items;
}

/**
 * Implements hook_image_default_styles().
 */
function commerce_sales_popup_image_default_styles() {
  $styles['commerce_sales_popup_modal'] = [
    'label' => 'Commerce sales popup (500x300)',
    'effects' => [
      [
        'name' => 'image_crop',
        'data' => [
          'width' => 500,
          'height' => 300,
          'upscale' => 1,
        ],
      ],
    ],
  ];
  $styles['commerce_sales_popup_header'] = [
    'label' => 'Commerce sales popup (500x50)',
    'effects' => [
      [
        'name' => 'image_crop',
        'data' => [
          'width' => 500,
          'height' => 50,
          'upscale' => 1,
        ],
      ],
    ],
  ];

  return $styles;
}

/**
 * Implements hook_node_view().
 */
function commerce_sales_popup_node_view($node, $view_mode, $langcode) {
  if ($node->type == SALES_ENTITY_TYPE) {
    $wrapper = entity_metadata_wrapper('node', $node);
    $id = $wrapper->getIdentifier();
    $preview = '#';

    // Add CSS.
    drupal_add_css(drupal_get_path('module', 'commerce_sales_popup') . '/css/commerce_sales_popup_extras.css');
    switch($wrapper->sales_display_type->value()) {
      case 'popup':
        $preview = '/commerce-sales-popup-extra/preview/' . $id . '/modal';
        break;
      case 'header':
        $preview = '/commerce-sales-popup-extra/preview/' . $id . '/header';
        break;
      case 'both':
        $preview = '/commerce-sales-popup-extra/preview/' . $id . '/both';
        break;
    }

    $publish = '/commerce-sales-popup-extra/publish/' . $id;
    $node->content['commerce_sales_extras'] = [
      '#weight' => -1,
      '#preview' => $preview,
      '#publish' => $publish,
      '#theme' => 'commerce_sales_extra_links',
    ];
  }
}

/**
 * Implements hook_node_presave().
 */
function commerce_sales_popup_node_presave($node) {
  if ($node->type == SALES_ENTITY_TYPE) {
    if ($node->is_new) {
      $node->status = 0;
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function commerce_sales_popup_form_commerce_sales_popup_node_form_alter(&$form, &$form_state, $form_id) {
  $output = '';
  $form['actions']['preview']['#access'] = FALSE;
  drupal_add_css(drupal_get_path('module', 'commerce_sales_popup') . '/css/commerce_sales_popup_extras.css');

  $output .= '<div class="commerce-sales-popup__notes">';
  $output .= '  <strong><em>Note:</em></strong>';
  $output .= '    <div>- On save, changes made will not be published immediately. Click Publish on view page to publish right away.</div>';
  $output .= '    <div>- Preview is only possible after saving this page.</div>';
  $output .= '</div>';

  $form['actions']['preview_info'] = [
    '#markup' => $output,
    '#weight' => -1,
  ];
}

/**
 * Check the following:
 * - sale entity has final date in future.
 * - sale time period has started already.
 * - load required js libraries, if both conditions are good.
 *
 * @param $wrapper
 *   Node entity meta data wrapper.
 *
 * @return boolean
 *   True if final date is in future.
 */
function commerce_sale_validate_period($wrapper) {
  $valid = FALSE;

  // Load js libraries only if we have a valid final date.
  if ($wrapper->__isset('sales_date')) {
    $dates = $wrapper->sales_date->value();

    // Check if this sale is valid - exoires in a future date/time.
    if (isset($dates['value2'])) {
      $start_date = $dates['value'];
      $final_date = $dates['value2'];

      if ((strtotime($start_date) < time()) && (time() < strtotime($final_date))) {
        libraries_load(COUNTDOWN_LIBRARY);
        drupal_add_js([
          'commerce_sales_popup' => [
            'final_date' => $final_date,
          ]
        ], 'setting');

        $settings = [
          'commerce_sales_popup' => [
            'timer_ticking' => theme('commerce_sales_timer_ticking'),
            'timer_elapsed' => theme('commerce_sales_timer_elapsed'),
          ],
        ];

        drupal_add_js($settings, 'setting');
        drupal_add_js(drupal_get_path('module', 'commerce_sales_popup') . '/js/commerce_sales_popup_start_countdown.js', [
          'scope' => 'footer',
          'weight' => 99,
        ]);

        $valid = TRUE;
      }
    }
  }

  return $valid;
}

/**
 * Menu callback to display a preview of sales popup.
 */
function commerce_sales_popup_preview($form, &$form_state, $node, $display_type) {
  $function = 'commerce_sales_display_popup_' . $display_type;
  $wrapper = entity_metadata_wrapper('node', $node);

  if (commerce_sale_validate_period($wrapper)) {
    $function($wrapper);
  }

  // Add CSS.
  drupal_add_css(drupal_get_path('module', 'commerce_sales_popup') . '/css/commerce_sales_popup_extras.css');

  $form['#title'] = t('@title preview', [
    '@title' => $node->title,
  ]);
  $form['prefix_markup'] = [
    '#markup' => '<div class="commerce-sales-popup__extra-links"><ul>'
  ];
  $form['edit_href'] = [
    '#type' => 'link',
    '#prefix' => '<li class="commerce-sales-popup__back">',
    '#title' => t('« Make changes'),
    '#href' => '/node/' . $node->nid . '/edit',
    '#options' => array(
      'html' => true,
    ),
    '#suffix' => '</li>',
  ];
  $form['publish_href'] = [
    '#type' => 'link',
    '#prefix' => '<li class="commerce-sales-popup__back">',
    '#title' => t('Publish »'),
    '#href' => '/commerce-sales-popup-extra/publish/' . $node->nid,
    '#options' => array(
      'html' => true,
    ),
    '#suffix' => '</li>',
  ];
  $form['desc_markup'] = [
    '#markup' => '</ul><div class="commerce-sales-popup__not-published">' . t('New changes made to sales popup information are not published yet. Click Publish to make them available.') . '</div>',
  ];
  $form['suffix_markup'] = [
    '#markup' => '</div>'
  ];

  $output = '';
  $output .= '<h1>Lorem Ipsum</h1>';
  $output .= '<h4>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</h4>';
  $output .= '<h5>"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</h5>';
  $output .= '<hr />';
  $output .= '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>';
  $output .= '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>';
  $output .= '<h3>1914 translation by H. Rackham</h3>';
  $output .= '<p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>';
  $output .= '<p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>';
  $output .= '<p>"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>';

  $form['lorem_ipsum'] = [
    '#markup' => $output,
    '#weight' => 99,
  ];

  return $form;
}

/**
 * Menu callback to publish a sales popup entity.
 */
function commerce_sales_popup_publish($form, &$form_state, $node) {
  $node->status = 1;
  node_save($node);

  // Rebuild cache bin to display this sales entity.
  db_query("DELETE FROM {cache};");
  drupal_set_message(t('Published.'), 'status');
  drupal_goto('/node/' . $node->nid);
}

/**
 * Modal page callback.
 */
function commerce_sales_popup_modal($js = NULL, $nid = NULL) {
  if ($nid == NULL) {
    return;
  }

  if ($js) {
    ctools_include('modal');
    ctools_include('ajax');
  }
  $node = node_load($nid);
  $content = theme('commerce_sales_popup_modal', ['node' => $node]);
  ctools_modal_render('', $content);
}

/**
 * Check if this is a desired path to display sales popup.
 *
 * @param array $mappings
 *   Display pages mapped to their sales entities.
 *
 * @return EntityDrupalWrapper|array
 *   Entity wrapper if current path matches.
 */
function is_desired_path($mappings) {
  $current_path = current_path();
  $display_pages = array_keys($mappings);

  // Exclude admin paths for * pattern.
  if (isset($mappings['*'])) {
    if (!path_is_admin($current_path)) {
      if (in_array($current_path, $display_pages)) {
        $mappings[$current_path] = array_merge($mappings[$current_path], $mappings['*']);
      }
      else {
        $mappings[$current_path] = $mappings['*'];
      }
      unset($mappings['*']);
    }
  }
  $display_pages = array_keys($mappings);

  return in_array($current_path, $display_pages) ? $mappings[$current_path] : [];
}

/**
 * Get display pages where sales popup will be shown.
 *
 * @return array
 *   Display pages mapped to their entities.
 */
function get_display_pages() {
  $cache = cache_get('sales_popup_display_pages');
  $mappings = [];

  if (empty($cache->data)) {
    $sales_entities = get_sales_entities(SALES_ENTITY_TYPE);
    if ($sales_entities) {
      $mappings = [];
      foreach ($sales_entities as $entity) {
        $wrapper = entity_metadata_wrapper('node', $entity);
        if ($wrapper->__isset('sales_popup_display_pages')) {
          if (!empty($wrapper->sales_popup_display_pages->value())) {
            $display_pages = $wrapper->sales_popup_display_pages->value();
            $pages = explode("\n", $display_pages);
            $pages = array_map(function ($page) {
              return trim($page);
            }, $pages);

            // Change to drupal path from alias, if given.
            foreach ($pages as $page) {
              // Make sure path '/', '<front>' and 'node' always point to front page URL.
              if ($page == '/' || $page == '<front>' || $page == 'node') {
                $page = variable_get('site_frontpage', 'node');
              }
              $internal_url = drupal_get_normal_path($page);
              $mappings[$internal_url][] = $wrapper;
            }
          }
        }
      }
      cache_set('sales_popup_display_pages', $mappings);
    }
  }
  else {
    $mappings = $cache->data;
  }

  return $mappings;
}

/**
 * Load all nodes of $type.
 *
 * @param string $type
 *   Node type.
 *
 * @return array
 *   Nodes entities of $type.
 */
function get_sales_entities($type) {
  $entities = [];
  $query = new EntityFieldQuery();
  $entity_ids = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $type)
    ->propertyCondition('status', NODE_PUBLISHED)
    ->execute();

  if (isset($entity_ids['node'])) {
    $entity_ids = array_keys($entity_ids['node']);
    $entities = node_load_multiple($entity_ids);
  }

  return $entities;
}

/**
 * Display sales popup as modal window.
 *
 * @param $wrapper
 *   Node entity wrapper.
 */
function commerce_sales_display_popup_modal($wrapper, $key = 0) {
  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  // Create our own javascript that will be used to theme a modal.
  $sales_popup = [
    'sales-popup-style' => [
      'modalSize' => [
        'type' => 'fixed',
        'width' => 500,
        'height' => 600,
        'addWidth' => 0,
        'addHeight' => 0,
        'contentRight' => 0,
        'contentBottom' => 0,
      ],
      'modalOptions' => [
        'opacity' => .8,
        'background-color' => '#000',
      ],
      'animation' => 'fadeIn',
      'modalClass' =>  'commerce-sales-popup__modal',
//      'throbber' => theme('image', ['path' => ctools_image_path('ajax-loader.gif', 'commerce_sales_popup'), 'alt' => t('Loading...'), 'title' => t('Loading')]),
//      'closeImage' => theme('image', ['path' => ctools_image_path('modal-close.png', 'commerce_sales_popup'), 'alt' => t('Close window'), 'title' => t('Close window')]),
    ],
  ];
  drupal_add_js($sales_popup, 'setting');
  ctools_add_css('commerce_sales_popup', 'commerce_sales_popup');

  // Trigger ctools modal directly on page load.
  $nid = $wrapper->getIdentifier();
  $classes = 'element-invisible commerce-sales-popup commerce-sales-popup--' . $nid;
  $popup_link = [
    '#access' => TRUE,
    '#prefix' => '<div class="' . $classes . '">',
    '#markup' => ctools_modal_text_button('Sales popup', 'commerce_sales_popup/nojs/' . $nid, t('sales'), 'ctools-modal-sales-popup-style'),
    '#suffix' => '</div>',
  ];

  $settings = [
    'commerce_sales_popup_modal' => [
      $key => [
        'popup_link' => drupal_render($popup_link),
        'node_id' => $nid,
      ]
    ],
  ];
  drupal_add_css(drupal_get_path('module', 'commerce_sales_popup') . '/css/commerce_sales_popup.css');
  drupal_add_js($settings, 'setting');
  drupal_add_js(drupal_get_path('module', 'commerce_sales_popup') . '/js/commerce_sales_popup_trigger.js');
}

/**
 * Display sales popup as floating header at the top.
 *
 * @param $wrapper
 *   Node entity wrapper.
 */
function commerce_sales_display_popup_header($wrapper, $key = 0) {
  drupal_add_css(drupal_get_path('module', 'commerce_sales_popup') . '/css/commerce_sales_popup.css');

  $settings = [
    'commerce_sales_popup_header' => [
      $key => [
        'nid' => $wrapper->getIdentifier(),
        'top_bar' => theme('commerce_sales_popup_header', [
          'node' => $wrapper->raw(),
        ]),
      ]
    ]
  ];

  drupal_add_js($settings, 'setting');
  drupal_add_js(drupal_get_path('module', 'commerce_sales_popup') . '/js/commerce_sales_popup_top_bar.js');
}

/**
 * Display sales popup as modal window and top header both.
 *
 * @param $wrapper
 *   Node entity wrapper.
 */
function commerce_sales_display_popup_both($wrapper) {
  commerce_sales_display_popup_modal($wrapper);
  commerce_sales_display_popup_header($wrapper);
}
